declare("TabReply", {
	PMFrom = {},
	Count = 0, -- To minimize latency in flight, Use this counter instead of #table to get the number of items in the table
	Last, -- holds the name of the last person to send a PM
	rHUDrun = false, -- has rHUDxscale occurred?
})

local chatentry_current = "HUD" -- will hold the key for the currently active chatentry
local chatentry_controls = {} -- will hold references to the various chatentry controls. keys [HUD|PDA|STA|CAP]

-- ---------------------------------------------------
-- Utility Functions
local function quote_wrap(s)
	return s:match("%s") and string.format('"%s"', s) or s
end

local function extract_name(s, cs)
	local q = cs and '"' or ''
	cs = cs and "%s+" or "" -- check for space
	return s:match('^/msg "([^"]+)'..q..cs..'$') or 
		   s:match('^/msg ([^"%s]+)'..cs..'$')
end
-- ---------------------------------------------------

function TabReply:GetName(str, dir)
	-- dir expects 1 or -1
	local i  = self.PMFrom[0]
	local replace = true
	
	if (i and extract_name(str, true)) then
		i = i + dir
		if (dir == 1 and i > self.Count) then i = 1
		elseif (dir == -1 and i < 1) then i = self.Count
		end
		self.PMFrom[0] = i
	elseif (str ~= "") then
		replace = false
	end

	return i and replace and string.format("/msg %s ", quote_wrap(self.PMFrom[i])) or str
end

-- ---------------------------------------------------
-- Hook the chatentry control to add functionality
local function hook_chatentry(HUDonly)

	local list = HUDonly and {HUD = {HUD.chatcontainer.chatentry}} or 
		{
			HUD = {HUD.chatcontainer.chatentry},
			PDA = {PDAChatArea[2][2], PDAChatArea},
			STA = {StationChatArea[2][2], StationChatArea},
			CAP = {CapShipLog.chatentry, CapShipChatTab}
		}

	for k, controls in pairs(list) do
		
		local chatentry = controls[1]
		local old_chatentry_action = chatentry.action

		function chatentry:action(ch, str)
			if (ch == 9) then -- Tab
				local newstr
				-- check if the caret is flush with a letter (/msg abc|) and tabcomplete the name
				if (extract_name(str)) then 
					newstr = tabcomplete(self.value, self.caret)
					-- if a successful tabcomplete but the function was called with a leading ", fix the ending
					if (newstr ~= self.value and newstr:sub(6, 6) == '"' and newstr:sub(-2) ~= '" ') then
						newstr = newstr:sub(1, -2)..'" '
					end
			
				else
					if (gkinterface.IsCtrlKeyDown()) then
						newstr = TabReply:GetName(self.value, 1)
					else
						newstr = TabReply:GetName(self.value, -1) -- descend through the list

					end
				end
				self.value = newstr
			elseif (ch == 13 or ch == 27) then -- Enter, Esc
				TabReply.PMFrom[0] = TabReply.PMFrom[TabReply.Last]
			end
			if (type(old_chatentry_action) == "function") then return old_chatentry_action(self, ch, str) end
		end

		if (k ~= "HUD") then
			local container = controls[2]
			local old_container_OnShow = container.OnShow
			function container:OnShow() 
				chatentry_current = k
				if (type(old_container_OnShow) == "function") then return old_container_OnShow(self) end
			end

			local old_container_OnHide = container.OnHide
			function container:OnHide() 
				chatentry_current = "HUD"
				if (type(old_container_OnHide) == "function") then return old_container_OnHide(self) end
			end
		end
		
		chatentry_controls[k] = chatentry
	end

end
-- ---------------------------------------------------

function TabReply:CHAT_MSG_PRIVATE(event, data)
	local name = data.name
	TabReply.Last = name
	
	if (not self.PMFrom[name]) then
		self.Count = self.Count + 1
		self.PMFrom[self.Count] = name
		self.PMFrom[name] = self.Count
	end
	
	-- Only adjust the current name index if not chatting
	local chatentry = chatentry_controls[chatentry_current]
	if (iup.GetFocus() ~= chatentry or self.Count == 1) then self.PMFrom[0] = self.PMFrom[name] end
end

function TabReply:PLAYER_ENTERED_GAME(event)
	-- Initialize the table in case we are mid-session and a PM has already been received before a reload
	local name = extract_name(tabcomplete("", 0), true)
	if (name) then self:CHAT_MSG_PRIVATE(nil, {name = name}) end
end

function TabReply:PLAYER_LOGGED_OUT(event)
	self.PMFrom = {}
	self.Count = 0
end

local rHUDxscale_Timer = Timer()
function TabReply:rHUDxscale(event)
	-- Use a Timer to ensure it goes after the HUD's event
	if (event) then 
		rHUDxscale_Timer:SetTimeout(1, function() hook_chatentry(true) end) 
	else
		hook_chatentry(false)
	end
	self.rHUDrun = true
end

RegisterEvent(TabReply, "CHAT_MSG_PRIVATE")
RegisterEvent(TabReply, "PLAYER_ENTERED_GAME")
RegisterEvent(TabReply, "PLAYER_LOGGED_OUT")
RegisterEvent(TabReply, "rHUDxscale")

-- Perform initial hook on all the chatentry controls
TabReply:rHUDxscale()
